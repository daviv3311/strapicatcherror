'use strict';

/**
 * camera service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::camera.camera', ({ strapi }) => ({

  async createCamerasFromPing(payload) {

    strapi.log.info('createCamerasFromPing')
    console.log('serv 1')
    try {
      await Promise.all(
        payload.connectivity.map(async (item) => {
          const data = {
            ipAddress: item.ipAddress,
            storeCode: payload.storeCode,
            lastContact: item.lastContact,
            status: item.status.toLowerCase() === 'connected',
            additionalAttributes: item.additionalAttributes,
            threshold: 2344234234234
          }

          await strapi.db.query('api::camera.camera').create({
            data: data,
          })
        })
      )
    } catch (err) {
     throw err
    } finally {
      console.log("APA buat")
    }
  },

  async findOneCamera(ip) {
    console.log('hello')
    try {
      console.log(ip)
      return await strapi.db.query('api::camera.camera').findOne({
        where: { ipAddress: ip },
      })
    } catch (err) {
      throw err
    }


  }
}))
